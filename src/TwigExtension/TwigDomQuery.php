<?php

namespace Drupal\twig_domquery\TwigExtension;

use Rct567\DomQuery\DomQuery;

/**
 * Class TwigDomQuery.
 */
class TwigDomQuery extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getTokenParsers() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeVisitors() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTests() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getOperators() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'twig_domquery';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('domQuery', [$this, 'domQuery']),
      new \Twig_SimpleFunction('domQuery_replace', [$this, 'domQueryReplace']),
      new \Twig_SimpleFunction('domQuery_remove', [$this, 'domQueryRemove']),
      new \Twig_SimpleFunction('domQuery_addClass', [
        $this,
        'domQueryAddClass',
      ]),
      new \Twig_SimpleFunction('domQuery_removeClass', [
        $this,
        'domQueryRemoveClass',
      ]),
      new \Twig_SimpleFunction('domQuery_append', [$this, 'domQueryAppend']),
      new \Twig_SimpleFunction('domQuery_prepend', [$this, 'domQueryPrepend']),
      new \Twig_SimpleFunction('domQuery_before', [$this, 'domQueryBefore']),
      new \Twig_SimpleFunction('domQuery_after', [$this, 'domQueryAfter']),
      new \Twig_SimpleFunction('domQuery_text', [$this, 'domQueryText']),
      new \Twig_SimpleFunction('domQuery_html', [$this, 'domQueryHtml']),
      new \Twig_SimpleFunction('domQuery_wrapAll', [$this, 'domQueryWrapAll']),
      new \Twig_SimpleFunction('domQuery_wrap', [$this, 'domQueryWrap']),
      new \Twig_SimpleFunction('domQuery_wrapInner', [$this, 'domQueryWrapInner']),
      new \Twig_SimpleFunction('domQuery_appendTo', [$this, 'domQueryAppendTo']),
      new \Twig_SimpleFunction('domQuery_prependTo', [$this, 'domQueryPrependTo']),
    ];
  }

  /**
   * Get domquery instance.
   *
   * @param string $html
   *   The html data.
   *
   * @return \Rct567\DomQuery\DomQuery
   *   DomQuery object.
   */
  public function domQuery($html) {
    if (is_object($html)) {
      $html = (string) $html;
    }
    return new DomQuery($html);
  }

  /**
   * Dom Query quick replace.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $replaceWith
   *   Content replace with.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryReplace($html, $selector, $replaceWith) {
    $dom = $this->domQuery($html);
    $dom->find($selector)->replaceWith($replaceWith);
    return (string) $dom;
  }

  /**
   * Dom Query quick remove.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryRemove($html, $selector) {
    $dom = $this->domQuery($html);
    $dom->find($selector)->remove();
    return (string) $dom;
  }

  /**
   * Dom Query quick add css.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Css Class to add.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryAddClass($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->addClass($data);
  }

  /**
   * Dom Query quick remove css.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Css Class to add.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryRemoveClass($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->removeClass($data);
  }

  /**
   * Dom Query quick prepend data.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to prepend.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryPrepend($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->prepend($data);
  }

  /**
   * Dom Query quick append data.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to append.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryAppend($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->append($data);
  }

  /**
   * Dom Query quick before data.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to before.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryBefore($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->before($data);
  }

  /**
   * Dom Query quick before data.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to before.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryAfter($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->after($data);
  }

  /**
   * Dom Query get text of selector.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryText($html, $selector) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->text();
  }

  /**
   * Dom Query get html of selector.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryHtml($html, $selector) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->html();
  }

  /**
   * Dom Query wrap all.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to before.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryWrapAll($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->wrapAll($data);
  }

  /**
   * Dom Query wrap.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to before.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryWrap($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->wrap($data);
  }

  /**
   * Dom Query wrap.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to before.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryWrapInner($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->wrapInner($data);
  }

  /**
   * Dom Query appendto.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to before.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryAppendTo($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->appendTo($data);
  }

  /**
   * Dom Query appendto.
   *
   * @param string $html
   *   The html.
   * @param string $selector
   *   The element selector.
   * @param string $data
   *   Data to before.
   *
   * @return string
   *   Return replaced content.
   */
  public function domQueryPrependTo($html, $selector, $data) {
    $dom = $this->domQuery($html);
    return (string) $dom->find($selector)->prependTo($data);
  }

}
