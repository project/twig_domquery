CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration


INTRODUCTION
------------

Allow alter html markup from twig with DomQuery library.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------


INSTALLATION
------------
Module required install with composer because it depend on package DomQuery
composer require 'drupal/twig_domquery'


CONFIGURATION
--------------

Use whole DomQuery object

- Requirement add custom settings to settings.php

```php
$settings['twig_sandbox_whitelisted_classes'] = [
  'Drupal\\Core\\Template\\Attribute',
  'Rct567\\DomQuery\\DomQuery'
];
```

Example for remove div has class field-label:

```tiwg
{% set dom = domQuery(form|render) %}
{% set _ = dom.find('.field-label').remove() %}
{{ dom.getOuterHtml|raw }}
```

Use support twig function without add custom settings

```twig
{{ domQuery_remove(form|render, "p:last")|raw }}
{{ domQuery_replace(form|render, "p:last", "<p>Last Item</p>")|raw }}
{{ domQuery_addClass(form|render, "p:last", "last-item")|raw }}
{{ domQuery_removeClass(form|render, "p.list", "list")|raw }}
```
